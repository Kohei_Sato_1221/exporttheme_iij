//
//
// ===== テーマスクリプト [Classic] =====
//
//

//
// === 初期実行 ===
//

$(function () {
    'use strict';

    //
    // === 共通 ===
    //
    var
        $window = $(window),
        $body = $('body'),
        slideSpeed = 'normal';

    //
    // === メニュー ===
    //

    // --- すべてを開く ---

    var
        classGroupExpanded = 'expanded';

    $('.btn-expand-menu')
        .click(function (e) {
            var newmode = $(this).attr('mode') ^ 1,
                opentext = newmode ? 'すべてを閉じる<i class="ti-angle-double-up"></i>' : 'すべてを開く<i class="ti-angle-double-down"></i>';

            if ($(this).attr('mode') === "0") {
                $('.nav-menu-list li.group')
                    .addClass(classGroupExpanded)
                    .children('ul')
                    .slideDown(slideSpeed);
            } else {
                $('.nav-menu-list li.group')
                    .children('ul')
                    .slideUp(slideSpeed, function () {
                        $('.nav-menu-list li.group').removeClass(classGroupExpanded);
                    });

            }
            $(this).html(opentext);
            $(this).attr('mode', newmode);
            return false;
        });

    // --- メニューの開閉 ---

    var
        classMenuClosed = 'status-menu-closed';
    $('.btn-view-switch')
        .click(function (e) {
            $body.toggleClass(classMenuClosed, !$body.hasClass(classMenuClosed));
            return false;
        });

    //
    // === ウィンドウ ===
    //

    // --- ウィンドウイベント ---

    var
        $navMenuContent = $('.nav-menu-content');
    var menutimer = false;
    $window
        // --- リサイズ ---
        .resize(function () {
            var subtractHeight = ($window.width() >= 768) ? 50 : 25;
            $('#page-menu').css('padding-top', $('#page-header').height());
            $navMenuContent
                .height($window.innerHeight() - $('#page-header').height() - subtractHeight);
            if (menutimer !== false) {
                clearTimeout(menutimer);
            }
            menutimer = setTimeout(function () {
                var windowWidth = window.innerWidth;
                // XSサイズであればメニューを閉じる
                if (windowWidth < 768) {
                    $('body').addClass('status-menu-closed');
                    $('#page-menu .ui-resizable-handle').hide();
                    $('#page-sequence').css('top', ($('#page-header').height()) + "px");
                    $('#page-sequence').css('bottom', "");
                } else {
                    $('body').removeClass('status-menu-closed');
                    var w = $('#page-menu').width();
                    $('#page-content').css('margin-left', (w + 20) + "px");
                    $('#page-sequence').css('top', "");
                    $('#page-sequence').css('bottom', "25px");
                    $('#page-sequence').css('padding-left', (w) + "px");
                    $('#page-menu .ui-resizable-handle').show();
                }
            }, 100);
        })
        .trigger('resize');

    // ページ内リンク //
    $('article a[href^="#"]').click(function () {
        var haederHeight = $('header').height();
        try {
            var href = $(this).attr("href");
            var target = $(href == "#" || href == "" ? 'body' : '[id="' + href.substring(1)
.replace(/\\/g,'\\\\')
.replace(/"/g,'\\"')
.replace(/'/g,"\\'")
.replace(/\./g,'\\.')
.replace(/\=/g,'\\=')
.replace(/\//g,'\\/')
.replace(/\+/g,'\\+')
.replace(/\!/g,'\\!')
.replace(/\?/g,'\\?')
.replace(/&/g,'\\&')+'"]');
            var position = target.offset().top - haederHeight - 10; //ヘッダの高さ分位置をずらす
            $("html, body").animate({ scrollTop: position }, 550, "swing");
        }
        catch (e) {
            console.error(e.message);
        }
        return true;
    });

    // ページ外リンク //
    $(window).on('load', function () {
        var haederHeight = $('header').height();
        try {
            var href = decodeURI($(location).attr('href'));
            var indexOfID = href.indexOf(".html#");
            if (indexOfID != -1) {
var href2 = href.substring(indexOfID + 6)
.replace(/\\/g,'\\\\')
.replace(/"/g,'\\"')
.replace(/'/g,"\\'")
.replace(/\./g,'\\.')
.replace(/\=/g,'\\=')
.replace(/\//g,'\\/')
.replace(/\+/g,'\\+')
.replace(/\!/g,'\\!')
.replace(/\?/g,'\\?')
.replace(/&/g,'\\&');
                var target = $('[id="'+href2+'"]');
                var position = target.offset().top - haederHeight - 10; //ヘッダの高さ分位置をずらす
                $("html, body").animate({ scrollTop: position }, 550, "swing");
                return false;
              }
          }
          catch (e) {
            console.error(e.message);
          }
    });

});

// --- eof ---
