//
//
// ===== 共通スクリプト =====
//
//

//
// === 初期実行 ===
//
$(function () {
    'use strict';
    //
    // === 共通 ===
    //
    var
        $window = $(window),
        $body = $('body'),
        $header = $('#page-header');

    //
    // --- メニューをセット ---
    //
    var setMenuList = function ($menu) {

        var
            $menuList = $menu.children('ul'),
            classGroup = 'group',
            classGroupExpanded = 'expanded',
            classActive = '.active',
            classNoGroup = 'nogroup',
            slideSpeed = 'fast';

        // --- グループの開閉 ---
        $menuList.find('li').each(function (i, elem) {
            if ($(elem).has('ul').length) {
                $(elem).addClass(classGroup).has(classActive).addClass(classGroupExpanded).children('ul').show();
                $(elem).children('a').before('<span class="icon has_child"></span>');
            } else {
                $(elem).addClass(classNoGroup);
                $(elem).children('a').before('<span class="icon bullet"></span>');
            }
        });

        $('#page-menu .nav-menu-list > li.group span.has_child').on('click', function () {
            var
                $parent = $(this).parent(),
                $childList = $parent.children('ul');
            if ($parent.hasClass(classGroupExpanded)) {
                $childList.slideUp(slideSpeed, function () {
                    $parent.removeClass(classGroupExpanded);
                });
            } else {
                $childList.slideDown(slideSpeed);
                $parent.addClass(classGroupExpanded);
            }
            return false;
        });
        // --- 子のリストのイベントバブリングを停止 ---

        $menuList
            .find('li li')
            .click(function (e) {
                e.stopPropagation();
            });

    };

    //
    // --- スクロールバーをセット ---
    //
    var setCustomScrollbar = function ($customScroll) {
        if ($.fn.mCustomScrollbar) {
            var
                themeName = $customScroll.attr('data-scroll-theme');

            themeName = themeName ? themeName : 'light';

            $customScroll
                .mCustomScrollbar({
                    axis: 'y',
                    theme: themeName,
                    autoHideScrollbar: false
                });
        };
    };

    //
    // --- スムーススクロール ---
    //
    if ($.fn.smoothScroll) {

        // --- ページ内 ---

        $('.to-page-top a')
            .smoothScroll({
                scrollTarget: 'body'
            });

        // --- ページが開いた時 ---

        if (location.hash) {
            try {
var href = decodeURIComponent(location.hash);
var href2 = href.substring(1)
.replace(/\\/g,'\\\\')
.replace(/"/g,'\\"')
.replace(/'/g,"\\'")
.replace(/\./g,'\\.')
.replace(/\=/g,'\\=')
.replace(/\//g,'\\/')
.replace(/\+/g,'\\+')
.replace(/\!/g,'\\!')
.replace(/\?/g,'\\?')
.replace(/&/g,'\\&');
                $.smoothScroll({
                    scrollTarget: '[id="'+href2+'"]'
                });
              }
              catch (e) {
                  console.error(e.message);
              }
        };

    };

    //
    //
    // === ウィンドウイベント ===
    //
    //
    var
        classScrollDown = 'status-scroll-down',
        classScrollUp = 'status-scroll-up';

    //
    // --- スクロール位置でクラスを変更（トップから一定量スクロールダウン） ---
    //

    if ($body.hasClass('doc-enable-scroll-down')) {

        var
            scrollThreshold = $header.height();

        $window
            .scroll(function () {
                var currentPos = $(this).scrollTop();
                if (currentPos > scrollThreshold) {
                    $body.addClass(classScrollDown);
                } else {
                    $body.removeClass(classScrollDown);
                };
            });
    };

    //
    // --- スクロール位置でクラスを変更（スクロールアップ／ダウン） ---
    //
    if ($body.hasClass('doc-enable-scroll-updown')) {

        var
            scrollStartPos = 0,
            scrollThreshold = 50,
            isScrollDown = false;

        $window
            .scroll(function () {
                var currentPos = $(this).scrollTop();
                if (Math.abs(scrollStartPos - currentPos) > scrollThreshold) {
                    if (currentPos > scrollStartPos) {
                        // 下にスクロール中
                        if (!isScrollDown) {
                            $body
                                .removeClass(classScrollUp)
                                .addClass(classScrollDown);
                        };
                        isScrollDown = true;
                    } else {
                        // 上にスクロール中
                        if (isScrollDown) {
                            $body
                                .removeClass(classScrollDown)
                                .addClass(classScrollUp);
                        };
                        isScrollDown = false;
                    };
                    scrollStartPos = currentPos;
                };
            });
    };

    //
    // === ピンチイン／アウト対応 ===
    //

    var
        classPinchOut = 'status-pinch-out';

    $body
        .on('gesturechange', function (event) {
            var
                orgEvent = event.originalEvent,
                scale = Math.round(orgEvent.scale * 100) / 100;

            if (scale > 1) {
                $body.addClass(classPinchOut);
            } else {
                $body.removeClass(classPinchOut);
            };

        });

    //
    //
    // === ファイルの読み込み ===
    //
    //

    //
    // --- メニューを読み込み ---
    //

    $('.wiki-toc').each(function () {

        var $this = $(this);

        // 特定のテーマでメニューの開閉機能をセット
        if ($body.hasClass('doc-enable-menu-slide')) {

            // XSサイズであればメニューを閉じる
            var windowWidth = window.innerWidth;
            if (windowWidth < 768) {
                $body.addClass('status-menu-closed');
            };

            setMenuList($this);

        };


        // [Stone] の場合メニューを半分にする
        if ($body.hasClass('doc-theme-stone')) {
            var
                $list = $this.children('ul').children('li'),
                listCount = $list.length;
            $list
                .eq(Math.ceil(listCount / 2))
                .prevAll()
                .addClass('first-column');
        };


        // カスタムスクロールをセット
        //setCustomScrollbar($('.custom-scroll-bar'));
        $('#page-menu').resizable({
            handles: "e", maxWidth: "768", minWidth: "100",
            resize: function (event, ui) {
                var windowWidth = window.innerWidth;
                if (windowWidth >= 768) {
                    $('#page-content').css('margin-left', ((ui.size.width-0) + 20) + "px");
                    $('#page-sequence').css('padding-left', (ui.size.width) + "px");
                }
            },
            stop: function (event, ui) {
                var windowWidth = window.innerWidth;
                if (windowWidth >= 768) {
                    $('#page-content').css('margin-left', ((ui.size.width-0) + 20) + "px");
                    $('#page-sequence').css('padding-left', (ui.size.width) + "px");
                }
            }
        });
    });
});
// --- eof ---
