
/**
*   last update 2015.11.10
*/

var Fess = (function(){

    var _baseUrl = Config.searchURL;
    var _filedsLabel = 'conf'; //Fess label

    var _start = 0;
    var _num = 20;
    var _fileType = 'filetype_s%3Ahtml';
    var _currentIndex = 0;
    var _keyword

    return {
        init : function(){
            _start = 0;
            _currentIndex = 0;
        },
        //初期検索
        doSearch: function( key, start )
        {
            _keyword = key;
            if(!start || typeof start == "undefined"){
                start = 0;
            }
            var searchQuery = $.trim(key);
            var urlBuf = [];
            var defer = $.Deferred();
            urlBuf.push( _baseUrl, encodeURIComponent(searchQuery),'&start=', start,'&fields.label=', _filedsLabel, '&num=', _num, '&additional=', _fileType );
            //console.log(urlBuf.join(""));
            $.ajax({
                type:'GET',
                url: urlBuf.join(""),
                dataType: 'json',
                cache: false,
                async: true,
                success: defer.resolve,
                error:defer.reject
            });
            return defer.promise();
        },
        //続きがある時
        next: function( )
        {
            _currentIndex++;
            var start = _num * _currentIndex;
            return this.doSearch( _keyword , start );
        },
        getCurrentIndex:function()
        {
            return _currentIndex;
        },
        getNum:function()
        {
            return _num;
        }
    }
}());


