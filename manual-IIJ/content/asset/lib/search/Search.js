
//for IE8
if (!Array.indexOf) {
    Array.prototype.indexOf = function(o) {
        for (var i in this) {
            if (this[i] == o) {
                return i;
            }
        }
        return -1;
    }
}

$.extend({
    htmlspecialchars: function htmlspecialchars(ch){
        ch = ch.replace(/&/g,"&amp;") ;
        //ch = ch.replace(/"/g,"&quot;") ;
        ch = ch.replace(/'/g,"&#039;") ;
        ch = ch.replace(/</g,"&lt;") ;
        ch = ch.replace(/>/g,"&gt;") ;
        return ch ;
    }
});


/**
*   last update 2016.04.25
*/
// Library
var Search = (function(){

    var _searchList = [];
    var _resultCnt = 0;
    var SUMMARY_LENGTH = 400;
    var _keyword = '';

    //結果を出力
    var _setResult = function( keyword )
    {
        _resultCnt = 0;
        var searchBoard = $('<div>').attr("id", "searchBoard");
        searchBoard.prepend( $('<div>').attr("id", "searchResult").addClass('loading') );
        $('#content *').remove();
        $('#content').prepend( searchBoard );
        $('#searchResult').append( '<h2 style="margin-bottom: 30px">keyword: <strong>'+ $('#page-search').val() +'</strong> [<span id="searchNum">0</span>件]</h2>');
        $('#searchResult').removeClass('loading');
        switch( Config.searchType ){
            case SEARCH_OFFLINE:
                _byLocal( keyword );
                break;
            case SEARCH_WEB:
                _byFess( keyword );
                break;
        }
    }

    var _byLocal = function( keyword )
    {
        var cnt = _searchList.length;
        for( var i = 0; i<cnt; i++){
            var fileURL = _searchList[i];
            var res = _searchLocal( keyword , fileURL, i);
            res.done(function(data, fileURL, i){
                _analysisHtml( data, keyword, fileURL , cnt , i);
                $('#searchResult').removeClass('loading');
            });
        }
    }

    var _byFess = function( keyword )
    {
        Fess.init();
        Fess.doSearch( keyword ).done( function(data){

            _appendResultForFess(data);
            Library.layout.resize();

            //pager出力
            if( $('#more').length == 0 && data.response.recordCount > data.response.pageSize )
            {
                var more = $('<div>').attr({'id':'more'}).append($('<div>').html('+').addClass('pagerMore') );
                more.append($('<div>').addClass('loading').html('<div class="double-bounce1"></div><div class="double-bounce2"></div>'));
                $('#searchBoard').append( more );
                more.click(function(){
                    $(this).addClass('connecting');
                    Fess.next().done(function(data){
                        var status = data.response.status;
                        switch( status ){
                            case 0:
                                _appendResultForFess( data );
                                break;
                            case 1:
                                $('#more').removeClass('connecting');
                                break;
                        }
                    });
                });
                $('#more div.pagerMore').on('inview', function(event, visible) {
                    if (visible) {
                        $('#more').addClass('connecting');
                        Fess.next().done(function(data){
                            var status = data.response.status;
                            switch( status ){
                                case 0:
                                    _appendResultForFess( data );
                                    break;
                                case 1:
                                    $('#more').removeClass('connecting');
                                    break;
                            }
                        });
                    }
                });
            }
        });
    }

    var _appendResultForFess = function( data )
    {
        var results = data.response.result;
        //console.info(JSON.stringify(data));
        //console.log(results);
        if( typeof results === "undefined" ){
            return false;
        }
        var cnt = results.length;
        var pageSize = data.response.pageSize;
        var pageNumber = data.response.pageNumber;

        for( var i =0; i<cnt; i++){
            var result = results[i];
            var listDom =  _getListDom( result.title ,result.urlLink, result.contentDescription , i + ( pageSize * (pageNumber-1) ) ) ;
            listDom.find('em').addClass('highlight');
            $('#searchResult').append( listDom );
        }

        //検索結果
        $('#searchNum').html( data.response.recordCount );

        if( (Fess.getCurrentIndex() +1) *  Fess.getNum() >= $('#searchNum').text() ){
            $('#more').addClass('nonActive').off();
        }else{
            $('#more').removeClass('connecting');
        }
    }

    //#nav ナビゲーション内のリンクを取得して検索対象とする
    var _getSearchList = function()
    {
        var list = [];
        $('ul.nav-menu-list a').each(function(){
            var href = $(this).attr('href');
            list.push(href);
        });
        return list;
    }

    var _getListDom = function( title, link , summary, idx)
    {
        var dom = $('<div>').addClass('searchList');
        dom.append( $('<a>').attr({'href': link ,'target': '_blank'}).html( '[ '+ (idx+1) +' ] '+ title ) );
        dom.append( $('<div>').addClass('summary').html(summary) )
        return dom;
    }

    // chrome では local ajaxができない
    //$ open -a Google\ Chrome --args --allow-file-access-from-files
    //ajaxにより、対象のfileURLにおいて、keywordを探す
    var _searchLocal = function ( keyword , fileURL, i )
    {
        var defer = $.Deferred();
        $.ajax({
            url: fileURL,
            dataType: "text",
            type: "GET",
            cache: false,
            success: function(data){ defer.resolve(data,fileURL, i) },
            error:defer.reject
        });
        return defer.promise();
    };

    var _analysisHtml = function( htmlText, keyword ,fileURL, cnt, myIndex)
    {

        parser = new DOMParser();
        //
        htmlDom = parser.parseFromString(htmlText, "text/html");
        var $html = $(htmlDom);
        //検索対象 jqueryObject
        var $innerHTML = $html.find('div.wiki-content');
        var innerHTML = $innerHTML.text();
        var keyList = _getKeyList( keyword );
        var has_match = true;

        for (var i=0; i < keyList.length; i++) {
            var pattern = '' + keyList[i] + ''
            pattern = pattern.replace( new RegExp( "\"(.*?)\"" , "g"), "$1" );

            /*
            //AND検索
            has_match = innerHTML.match( new RegExp( pattern, 'gi'), _replacer );
            if( has_match ){
                innerHTML = innerHTML.replace( new RegExp( pattern, 'gi'), _replacer );
            }else{
                //return false;
            }
            */

            if( pattern.indexOf('-') == 0 ){

                pattern = pattern.replace('-', '');
                var result = innerHTML.match( new RegExp( pattern, 'gi'), _replacer );
                //除外検索、検索結果があったら、終了
                if( keyList.length == 1 || result){
                    has_match = false;
                    return false;
                }
            }else{
                //AND検索
                has_match = innerHTML.match( new RegExp( pattern, 'gi'), _replacer );
                if( has_match ){
                    innerHTML = innerHTML.replace( new RegExp( pattern, 'gi'), _replacer );
                }else{
                    //return false;
                }
            }
        }

        /////////////////////////////////////////////////////////////////////////////////
        //検索文字列は以後、存在する前提
        if( has_match ){

            var indexOf = innerHTML.indexOf('<em class="highlight">');
            var title = $html.find('h2#title-text').text();
            if( title == "" ){
                title = "no title";
            }

            var resultIdx = $('#searchResult div.searchList').length;

            $('#searchResult').append(
                _getListDom( title , fileURL, innerHTML.slice( indexOf, indexOf + SUMMARY_LENGTH ), resultIdx ) 
            );

            data = null;    //解放
            //Library.layout.resize();
            _resultCnt++;

            //検索結果
            $('#searchNum').html( _resultCnt );
        }

        if( myIndex == cnt-1 ){
            //alert('検索完了しました');
            $('#searchResult').slideDown();
        }
    }

    var _getReplaceTag = function( $obj )
    {
        return $obj.html().replace( /<em class="highLight">([\s\S]*?)<\/em>/gi , "_STARTKEY_$1_ENDKEY_" ).replace(/<("[^"]*"|'[^']*'|[^'">])*>/g,'');
    };

    var _removeTag = function(str, arrowTag) {
        // 配列形式の場合は'|'で結合
        if ((Array.isArray ?
                Array.isArray(arrowTag)
                : Object.prototype.toString.call(arrowTag) === '[object Array]')
        ) {
                arrowTag = arrowTag.join('|');
        }
        // arrowTag が空の場合は全てのHTMLタグを除去する
        arrowTag = arrowTag ? arrowTag : '';
        // パターンを動的に生成
        var pattern = new RegExp('(?!<\\/?(' + arrowTag + ')(>|\\s[^>]*>))<("[^"]*"|\\\'[^\\\']*\\\'|[^\\\'">])*>', 'gim');
        return str.replace(pattern, '');
    };

    var _replacer = function( str, offset, s )
    {
        var greater = s.indexOf( '>', offset );
        var lesser = s.indexOf( '<', offset );
        if( greater < lesser || ( greater != -1 && lesser == -1 ) )
        {
            return str;
        }
        else
        {
            return '<em class="highlight">' + str +'</em>';
        }
    };
    var _keyWordCheck = function( keyword )
    {

        if(Config.searchType == SEARCH_OFFLINE){
            keyword = keyword.replace(/\^/g,'');
            keyword = keyword.replace(/\&/g,' ');
            keyword = keyword.replace(/^\*+|\*+$/g, "");
            keyword = keyword.replace(/　/g,' ');
            keyword = keyword.replace(/\s+/g ,' ');
            keyword = keyword.replace(/^\s+|\s+$/g, "");
            keyword = keyword.replace(/\[/g, "\\[");
            keyword = keyword.replace(/\]/g, "\\]");
            keyword = keyword.replace(/\*+/g, "*");
            keyword = keyword.replace(/\*/g, "[\\s\\S].{0,10}");
            keyword = keyword.replace(/\(/g, "\\(");
            keyword = keyword.replace(/\)/g, "\\)");
            keyword = keyword.replace(/\?/g, "\\?");
            keyword = keyword.replace(/\|/g, "\\|");
        }else{
            keyword = $.htmlspecialchars(keyword);
            keyword = keyword.replace(/\^/g,'');
            keyword = keyword.replace(/\&/g,' ');
            keyword = keyword.replace(/^\*+|\*+$/g, "");
            keyword = keyword.replace(/　/g,' ');
            keyword = keyword.replace(/\s+/g ,' ');
            keyword = keyword.replace(/^\s+|\s+$/g, "");
            keyword = keyword.replace(/\*+/g, "*");
        }
        _keyword = keyword;
        return keyword
    };
    var _getKeyList = function( keyword )
    {
        var list = keyword.match(/(?:[^\s"]+|"[^"]*")+/g);
        return list
    };
    var _set_is_searchresult_flag = function()
    {
        $.localStorage.get('is_searchResult');
        $.localStorage.set( 'is_searchResult', true );
    };
    var _remove_is_searchresult_flag = function()
    {
        $.localStorage.set( 'is_searchResult', false );
    };
    var _saveSearchWord = function()
    {
        var $searchField = $('#page-search input');
        var val = $searchField.val();
        $.localStorage.set( 'searchWord', val );
    };
    var _setHighLight = function( word )
    {

        var $target = $('#page-content');
        var html =  $target.html();
        if( html ){
            var match = html.match( new RegExp( word, 'i') );

            if( match ){
                var originalKey = html.match( new RegExp( word, 'i') )[0];
                html= html.replace( new RegExp( word, 'gi'), _replacer );
                $target.html(html);
            }
        }
    }

    //public
    return{
        resultCnt:0,

        //レイアウトとクリックイベントを設定
        init:function()
        {

            var searchButton = $('#page-search button');
            var searchField = $('#page-search input');

            _searchList = _getSearchList();
            var my = this;
           searchButton.click(function(e){

                e.preventDefault();
                window.scrollTo( 0, 0 );
                $('#searchBoard *').remove();
                $('#main-header h1').html('検索結果');
                $('#breadcrumb-section').remove();
                //Library.layout.hideNav( 300, function(){
                    var val = searchField.val();
                    var key =  _keyWordCheck(val);
                    if( !key.match(/[0-9a-zA-ZＡ-Ｚぁ-んァ-ン一-龥]/) ){
                        alert('検索キーワードをいれてください');
                    }else{
                        _setResult( key );
                        $('#pager').remove();
                    }
                //});
            });

            //検索結果のリストをクリック時、キーワードをストレージに格納する
            $(document).on("click", "#searchResult a", (function(){
                _set_is_searchresult_flag();
                _saveSearchWord();
            }));
        },
        //検索結果後、ハイライト表示ページ
        //検索結果から訪問しているかどうかの確認
        showResult :function()
        {

            var my = this;
            var $searchField = $('#page-search input');
            //this._set_is_searchresult_flag();

            if( $.localStorage.get('is_searchResult') ){

                //検索ハイライト表示
                var searchWord = $.localStorage.get( 'searchWord' );
                $searchField.val( searchWord );


                //検索ワードリスト作成
                var keyList = _getKeyList( _keyWordCheck(searchWord) );

                //ハイライト処理
                for (var i=0; i < keyList.length; i++) {
                    _setHighLight( keyList[i] );
                }

                //スクロール
                if( $('em.highlight').length>0){
                    var targetPositionTop = $('em.highlight').offset().top;
                    $('body,html').stop().animate({
                      scrollTop: targetPositionTop - $('#page-header').height() * 1.3
                    }, 500);
                }
                //検索結果フラグ消す
                _remove_is_searchresult_flag();
            }
        }
    }

//
}());

$(function(){
    $(window).load(function(){

    //検索動作
    Search.init();
    //検索結果から訪問している場合は、ハイライト処理
    Search.showResult();


    });

});

